var config = {
	develop: true,
	prodTemplate: 	'<div class="product-card _extended">												\
						<header class="product-card__header">											\
							<a href="" class="product-card__name js-product-link" id="prod-name"></a>	\
							<div class="product-card__manufacturer" id="prod-manufacturer"></div>		\
						</header>																		\
																										\
						<section class="product-card__image js-product-image" id="prod-img"></section>	\
																										\
						<section class="product-card__info">											\
							<div class="product-card__specs">											\
								product-card__specs														\
							</div>																		\
							<div class="product-card__bottom">											\
								<div class="product-card__price" id="prod-price"></div>					\
								<div class="product-card__sellers">										\
								</div>																	\
								<div class="product-card__actions">										\
									<span class="product-card__action">									\
										<span class="symbol _compare"></span>							\
									</span>																\
									<span class="product-card__action">									\
									</span>																\
								</div>																	\
							</div>																		\
						</section>																		\
					</div>',
	path: 			'/catalog/product-model',
	catalogCont: 	'data-items-container',
	filtersCont: 	'data-filter-id',
	filterVal: 		'data-filter-value-id',
	rangeSelector: 	'data-range',
	sortCont: 		'data-sort-container',
	sortItem: 		'data-sort-item',
	pageCont: 		'pagination'
};

var currentEl;

var params = { };

var catalogCont = document.querySelector('['+config.catalogCont+']');
var filtersCont = document.querySelectorAll('['+config.filtersCont+']');
document.addEventListener('DOMContentLoaded', initPaginator(), false);

function priceOnBlur(element) {

	if(!element['value']) {
		runBundle();
		return;
	}

	switch(element.getAttribute('data-range')) {
		case "min":
			element['value'] < element['placeholder'] ? element['value'] = element['placeholder'] : element['value'];
			break;

		case "max":
			parseInt(element['value']) > parseInt(element['placeholder']) ? element['value'] = element['placeholder'] : element['value'];
			break;		
	};
	
	document.getElementById('pagination').setAttribute('data-page-current', 1);
	initPaginator();

	runBundle();
}

function setSortMethod(element) {
	var parent = document.querySelector('['+config.sortCont+']');
	var method = parent.getAttribute(config.sortCont);
	switch (element.getAttribute(config.sortItem)) {
		case 'price':
			method == 'price' ? parent.setAttribute(config.sortCont, '-price') : parent.setAttribute(config.sortCont, 'price');
			method == 'price' ? element.className = 'sorting-item _two-way is-active is-reversed' : element.className = 'sorting-item _two-way is-active';
			break;
	};

	runBundle();

}

function filterClick(element) {

	if(currentEl == element) { 
		currentEl = undefined; 
		return;
	};

	currentEl = element;

	if(config.develop) console.log(element);

	var filterName = element.parentNode.getAttribute(config.filtersCont);
	var filterVal = element.getAttribute(config.filterVal);

	setFilterCheck(element);

	document.getElementById('pagination').setAttribute('data-page-current', 1);
	initPaginator();

	runBundle();

}

function runBundle() {
	params = {};
	checkFilterValues(document.querySelectorAll('['+config.filtersCont+']'));
	checkInputValues(document.querySelectorAll('['+config.filtersCont+']'));
	checkSortValues(document.querySelector('['+config.sortCont+']'));
	checkPage(document.getElementById(config.pageCont));
	if(config.develop) console.log(params);
	sendGETRequest(config.path, params)
}

function setFilterCheck(element) {
	var child = element.querySelector('input[type=checkbox]');
	child.getAttribute('data-type') == 'set' ? child.setAttribute('data-type', 'unset') : child.setAttribute('data-type', 'set');
};

function checkSortValues(item) {
	var value = item.getAttribute(config.sortCont);
	if(value) params['sort'] = value;
};

function checkFilterValues(items) {
	[].forEach.call(items, function(node) {
		var filterName = node.getAttribute(config.filtersCont);
		var values = node.querySelectorAll('['+config.filterVal+']');
		getFilterValues(filterName, values);
	});
};

function getFilterValues(filter_name, childrens) {
	[].forEach.call(childrens, function(node) {
		var value = node.getAttribute(config.filterVal);
		var element = node.querySelector('input');
		element['type'] && element['type'] == 'checkbox' ? getFilterValuesCheck(filter_name+'__in', value, element) : null;
	});
};

function getFilterValuesCheck(filter_name, value, element) {
	if(element.getAttribute('data-type') == 'set' && params[filter_name]) { params[filter_name] +=', '+value  };
	if(element.getAttribute('data-type') == 'set' && !params[filter_name]) { params[filter_name] = ''+value };
};

function checkInputValues(items) {
	[].forEach.call(items, function(node) {
		var filterName = node.getAttribute(config.filtersCont);
		var childrens = node.querySelectorAll('input');
		getInputs(filterName, childrens);
	});
}

function getInputs(filter_name, items) {
	[].forEach.call(items, function(node) {
		node['type'] && node['type'] == 'text' ? getInputValue(filter_name, node['value'], node.getAttribute(config.rangeSelector)) : null;
	});	
}

function getInputValue(filter_name, value, selector) {
	if(!value || !selector) return;

	filter_name += '__range';

	if(!params[filter_name]) {
		if(selector == 'min') { params[filter_name] = value+'..' } else if(selector == 'max') { params[filter_name] = '..'+value };
	}
	else {
		if(selector == 'min') { params[filter_name] = value+params[filter_name] } else if(selector == 'max') { params[filter_name] += value };
	};
};

function checkPage(element) {
	var currentPage = document.querySelector('[data-current-page]');
	params['page'] = currentPage.getAttribute('data-page-link');
}

function sendGETRequest(path, params) {
	createPreloader(catalogCont);
	var container =	document.querySelector('['+config.catalogCont+']');
	var URL = setParams(path, params);
	if(config.develop) console.log(URL);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() {
		if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			destroyPreloader('preloader');
			changeURLAddress('?'+URL.split('?')[1]);
			var items = JSON.parse(xmlHttp.responseText);
			if(config.develop) console.log(items);
			setItems(container, items['items']);
			findFilters(document.querySelectorAll('['+config.filtersCont+']'), items['filters']);
			if(items['pager']['totalCount'] > 0 && items['pager']['pageCount'] > 1) {
				document.getElementById('pagination').removeAttribute('style');
				parseInt(document.getElementById('pagination').setAttribute('data-page-count', items['pager']['pageCount']));
				parseInt(document.getElementById('pagination').setAttribute('data-page-current', items['pager']['page']));
				initPaginator();
			}
			else {
				document.getElementById('pagination').style.display = 'none';
			};
		};
	};
	xmlHttp.open("GET", URL, true);
	xmlHttp.send('');
};

function setParams(path, params) {
	var count = 0;
	for(var key in params) {
		if(!params[key]) continue;
		count++;
		var value = params[key];
		key = key.replace(/([a-z])([A-Z])/g, "$1-$2");
		key = key.toLowerCase();
		if(key == 'sort') {
			value = value.replace(/([a-z])([A-Z])/g, "$1-$2");
			value = value.toLowerCase();
		};
		count == 1 ? path += '?'+key+'='+value : path += '&'+key+'='+value;
	};
	return path;
};

function setItems(container, items) {
	clearCatalogTree(container);
		for(var key in items) {
			createElement(container, items[key]);
		};
};

function createElement(container, item) {
	var div = document.createElement('div');
	div.className += 'product-list__item';
	div.innerHTML = config.prodTemplate;
	container.appendChild(div);

	var prodName = item.properties.name.value+' / '+item.properties.vendorCode.value;
	var manufacturer = '';
	for(var i = 0; i < item.properties.countryCode.value.length; i++) {
		i != item.properties.countryCode.value[i].length-2 ? manufacturer += item.properties.countryCode.value[i]+', ' : manufacturer += item.properties.countryCode.value[i];
	};
	var price = item.properties.price.value.slice(0, -2)+' руб.';
	var img = 'url('+item.properties.imageUrls[0]+')';

	findChildById(div, 'prod-name', prodName, null);
	findChildById(div, 'prod-manufacturer', manufacturer, null);
	findChildById(div, 'prod-price', price, null);
	findChildById(div, 'prod-img', img, 'backgroundImage');
};

function clearCatalogTree(container) {
	container.innerHTML = '';
};

function findChildById(parent, id, value, style) {
	var childes = parent.childNodes;
	if(childes.length < 1) return;
	for(var i = 0; i < childes.length; i++) {
		if(childes[i]['id'] == id) { style ? childes[i].style[style] = value : childes[i].innerHTML = value }
		else { findChildById(childes[i], id, value, style) };
	};
};

function createPreloader(container) {
	var div = document.createElement('div');
	div.setAttribute('id', 'preloader');
	div.innerHTML = '<div id="loader"><div id="shadow"></div><div id="box"></div></div>';
	div.style.position = 'absolute';
	div.style.backgroundColor = 'rgba(255,255,255,.9)';
	div.style.left = '0';
	div.style.top = '0';
	div.style.width = '100%';
	div.style.height = '100%';
	div.style.zIndex = '9999';
	div.style.margin = '0 auto';
	container.appendChild(div);
}

function destroyPreloader(id) {
	var element = document.getElementById(id);
	element.remove();
}

function findFilters(items, filters) {
	[].forEach.call(items, function(node) {
		var currentFilter = node.getAttribute(config.filtersCont);
		for (var i = filters.length - 1; i >= 0; i--) {
			if(filters[i]['sysname'] == currentFilter) {
				var filterValues = node.querySelectorAll('['+config.filterVal+']');
				findFilterValues(currentFilter, filterValues, filters[i]);
			}
		}
	});
};

function findFilterValues(current_filter, filter_values, filter) {
	[].forEach.call(filter_values, function(node) {

		var elements = node.getElementsByClassName('highlight');
		elements[0].innerHTML = '&nbsp;(0 шт.)&nbsp;'

		for (var i = filter.values.length - 1; i >= 0; i--) {
			if(filter.values[i].value == node.getAttribute(config.filterVal)) {
				elements[0].innerHTML = '&nbsp;('+filter.values[i].count+' шт.)&nbsp;';
			};
		};

	});
};

function changeURLAddress(url_path) {
	window.history.pushState("Catalog", document.title, url_path);
};