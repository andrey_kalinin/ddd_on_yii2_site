<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 19:22
 */

namespace backend\modules\core\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\core\elementClass as elementClassActions;
use commonprj\components\core\entities\elementClass\ElementClass;

/**
 * Class ElementClassController
 * @package backend\controllers
 */
class ElementClassController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = ElementClass::class;

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'index'                            => [
                'class'       => 'common\extendedStdComponents\IndexAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view'                             => [
                'class'       => 'common\extendedStdComponents\ViewAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createElementClass2Property' => [
                'class'       => elementClassActions\CreateElementClass2PropertyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteElementClass2Property' => [
                'class'       => elementClassActions\DeleteElementClass2PropertyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementClassContext' => [
                'class'       => elementClassActions\ViewElementClassContextAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementClassProperties' => [
                'class'       => elementClassActions\ViewElementClassPropertiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementClassRelationClasses' => [
                'class'       => elementClassActions\ViewElementClassRelationClassesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}