<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06.02.2018
 * Time: 18:48
 */

namespace backend\modules\core\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\core\property as PropertyActions;
use commonprj\components\core\entities\property\Property;

/**
 * Class PropertyController
 * @package backend\modules\core\controllers
 */
class PropertyController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = Property::class;

    /**
     * @return array
     */
    public function addActions(): array
    {
        return [
            'create'                     => [
                'class'       => PropertyActions\CreatePropertyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'index'                     => [
                'class'       => PropertyActions\IndexPropertyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyValues'          => [
                'class'       => PropertyActions\ViewPropertyValuesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyElementClasses' => [
                'class'       => PropertyActions\ViewPropertyElementClassesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyVariants'       => [
                'class'       => PropertyActions\ViewPropertyVariantsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyListItems'      => [
                'class'       => PropertyActions\ViewPropertyListItemsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createPropertyListItems'    => [
                'class'       => PropertyActions\CreatePropertyListItemsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}