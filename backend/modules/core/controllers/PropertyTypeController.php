<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.02.2018
 * Time: 13:47
 */

namespace backend\modules\core\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\core\property\ViewPropertyGroupOrPropertyUnitPropertiesAction;
use commonprj\components\core\entities\propertyType\PropertyType;

/**
 * Class PropertyTypeController
 * @package backend\controllers
 */
class PropertyTypeController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = PropertyType::class;

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'index'                                     => [
                'class'       => 'common\extendedStdComponents\IndexAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view'                                      => [
                'class'       => 'common\extendedStdComponents\ViewAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyGroupOrPropertyUnitProperties' => [
                'class'       => ViewPropertyGroupOrPropertyUnitPropertiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}