<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.04.2018
 * Time: 13:29
 */

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\materialGroup\ViewMaterialGroupsWithMaterialsAction;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;

class MaterialGroupWithMaterialsController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = MaterialGroup::class;

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class'       => ViewMaterialGroupsWithMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}