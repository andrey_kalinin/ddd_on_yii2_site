<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.02.2018
 * Time: 14:04
 */

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\materialGroup\ViewMaterialGroupMaterialsAction;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;

/**
 * Class MaterialGroupController
 * @package backend\modules\catalog\controllers
 */
class MaterialGroupController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = MaterialGroup::class;

    /**
     * @return array
     */
    public function addActions(): array
    {
        return [
            'viewMaterialGroupMaterials' => [
                'class'       => ViewMaterialGroupMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}