<?php

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\priceCategory as PriceCategoryAction;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;


/**
 * Class PriceCategoryController
 * @package api\controllers
 */
class PriceCategoryController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = PriceCategory::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'create' => [
                'class'       => PriceCategoryAction\CreatePriceCategoryAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationPriceCategory2Manufacturer' => [
                'class'       => PriceCategoryAction\CreateRelationPriceCategory2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationPriceCategory2Manufacturer' => [
                'class'       => PriceCategoryAction\DeleteRelationPriceCategory2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewManufacturer' => [
                'class'       => PriceCategoryAction\ViewManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}