<?php

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use commonprj\components\core\entities\elementCategory\ElementCategory;

/**
 * Class ProductMaterialController
 * @package api\controllers
 */
class ProductModelCategoryController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = ElementCategory::class;

}