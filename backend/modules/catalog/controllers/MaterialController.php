<?php

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\material as MaterialAction;
use commonprj\components\catalog\entities\material\Material;


/**
 * Class MaterialController
 * @package api\controllers
 */
class MaterialController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = Material::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'create'                                     => [
                'class'       => MaterialAction\CreateMaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createAndBindMaterialTemplate'              => [
                'class'       => MaterialAction\CreateAndBindMaterialTemplateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationMaterial2MaterialGroup'       => [
                'class'       => MaterialAction\CreateRelationMaterial2MaterialGroupAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationMaterial2MaterialGroup'       => [
                'class'       => MaterialAction\DeleteRelationMaterial2MaterialGroupAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewMaterialGroup'                          => [
                'class'       => MaterialAction\ViewMaterialGroupAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationMaterial2MaterialTemplate'    => [
                'class'       => MaterialAction\CreateRelationMaterial2MaterialTemplateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationMaterial2MaterialTemplate'    => [
                'class'       => MaterialAction\DeleteRelationMaterial2MaterialTemplateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewMaterialTemplate'                       => [
                'class'       => MaterialAction\ViewMaterialTemplateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewComponents'                             => [
                'class'       => MaterialAction\ViewComponentsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProductModels'                          => [
                'class'       => MaterialAction\ViewProductModelsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationMaterial2CompositionMaterial' => [
                'class'       => MaterialAction\CreateRelationMaterial2CompositionMaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationMaterial2CompositionMaterial' => [
                'class'       => MaterialAction\DeleteRelationMaterial2CompositionMaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompositionMaterials'                   => [
                'class'       => MaterialAction\ViewCompositionMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompositionParents'                     => [
                'class'       => MaterialAction\ViewCompositionParentsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationMaterial2HierarchyChild'      => [
                'class'       => MaterialAction\CreateRelationMaterial2HierarchyChildAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewHierarchyChildren'                      => [
                'class'       => MaterialAction\ViewHierarchyChildrenAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewHierarchyParent'                        => [
                'class'       => MaterialAction\ViewHierarchyParentAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewHierarchyDescendants'                   => [
                'class'       => MaterialAction\ViewHierarchyDescendantsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationMaterial2HierarchyChild'      => [
                'class'       => MaterialAction\DeleteRelationMaterial2HierarchyChildAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}