<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.03.2018
 * Time: 11:25
 */

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\productMaterial\ViewPropductMaterialExtendedAction;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;

/**
 * Class ProductMaterialExtendedController
 * @package backend\controllers
 */
class ProductMaterialExtendedController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = ProductMaterial::class;

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class'       => ViewPropductMaterialExtendedAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}