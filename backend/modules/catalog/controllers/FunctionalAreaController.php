<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.04.2018
 * Time: 18:48
 */

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\functionalArea\CreateFunctionalAreaAction;
use common\extendedStdComponents\catalog\functionalArea\IndexFunctionalAction;
use common\extendedStdComponents\UpdateAction;
use common\extendedStdComponents\ViewAction;
use commonprj\components\core\entities\PropertyTreeItem\PropertyTreeItem;

/**
 * Class FunctionalAreaController
 * @package backend\modules\catalog\controllers
 */
class FunctionalAreaController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = PropertyTreeItem::class;

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'create' => [
                'class'       => CreateFunctionalAreaAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'index' => [
                'class'       => IndexFunctionalAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'update' => [
                'class'       => UpdateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class'       => ViewAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}