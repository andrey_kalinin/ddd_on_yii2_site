<?php

namespace backend\modules\crm\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\crm\address as AddressAction;
use commonprj\components\crm\entities\address\Address;


/**
 * Class AddressController
 * @package api\controllers
 */
class AddressController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = Address::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'create' => [
                'class'       => AddressAction\CreateAddressAndBindToOwnerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}