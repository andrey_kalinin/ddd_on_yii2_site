<?php

namespace backend\modules\crm\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\crm\employee as EmployeeAction;
use commonprj\components\crm\entities\employee\Employee;


/**
 * Class EmployeeController
 * @package api\controllers
 */
class EmployeeController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = Employee::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'create' => [
                'class'       => EmployeeAction\CreateEmployeeAndBindCompanyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationEmployee2Company' => [
                'class'       => EmployeeAction\CreateRelationEmployee2CompanyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationEmployee2Company' => [
                'class'       => EmployeeAction\DeleteRelationEmployee2CompanyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompany' => [
                'class'       => EmployeeAction\ViewCompanyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProductOffers' => [
                'class'       => EmployeeAction\ViewProductOffersAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationEmployee2Address' => [
                'class'       => EmployeeAction\CreateRelationEmployee2AddressAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationEmployee2Address' => [
                'class'       => EmployeeAction\DeleteRelationEmployee2AddressAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewAddresses' => [
                'class'       => EmployeeAction\ViewAddressesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewAccessSpecification' => [
                'class'       => EmployeeAction\ViewAccessSpecificationAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationEmployee2UserRole' => [
                'class'       => EmployeeAction\CreateRelationEmployee2UserRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationEmployee2UserRole' => [
                'class'       => EmployeeAction\DeleteRelationEmployee2UserRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewUserRole' => [
                'class'       => EmployeeAction\ViewUserRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}