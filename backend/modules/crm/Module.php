<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 04.07.2016
 */

namespace backend\modules\crm;

/**
 * Class Module
 * @package api\modules\core
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
    }
}