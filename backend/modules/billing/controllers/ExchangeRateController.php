<?php

namespace backend\modules\billing\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\HttpException;
use commonprj\extensions\CustomHttpClient;

/**
 * Class AddressController
 * @package api\modules\crm\controllers
 */
class ExchangeRateController extends Controller
{
    public function actionIndex()
    {
        /** @var CustomHttpClient $client */
        $client = Yii::$app->crmRestServer;
        $uri = 'billing/exchange-rate';
        $response = $client->get($uri)->send();

        if (!$response->getIsOk()) {
            $content = $response->getData();
            $message = (!empty($content)) ? ($content['message'] ?? '') : '';
            throw new HttpException($response->getStatusCode(), $message);
        }

        $content = $response->getData();

        return $content;
    }

}