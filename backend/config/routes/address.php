<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'crm/address',
            ],
            'pluralize'     => false,
];
