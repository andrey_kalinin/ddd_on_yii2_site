<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'catalog/product-material',
        ],
        'tokens'        => [
            '{id}'        => '<id:\\d+>',
            '{elementId}' => '<elementId:\\d+>',
        ],
        'extraPatterns' => [
            'GET {id}/manufacturer'        => 'viewManufacturer',
            'GET {id}/material-group'      => 'viewMaterialGroup',
            'GET {id}/material'            => 'viewMaterial',
            'GET {id}/material-collection' => 'viewMaterialCollection',
            'GET {id}/price-category'      => 'viewPriceCategory',

            'POST {id}/manufacturer/{elementId}'        => 'createRelationProductMaterial2Manufacturer',
            'POST {id}/material/{elementId}'            => 'createRelationProductMaterial2Material',
            'POST {id}/material-collection/{elementId}' => 'createRelationProductMaterial2MaterialCollection',
            'POST {id}/price-category/{elementId}'      => 'createRelationProductMaterial2PriceCategory',

            'DELETE {id}/manufacturer/{elementId}'        => 'deleteRelationProductMaterial2Manufacturer',
            'DELETE {id}/material/{elementId}'            => 'deleteRelationProductMaterial2Material',
            'DELETE {id}/material-collection/{elementId}' => 'deleteRelationProductMaterial2MaterialCollection',
            'DELETE {id}/price-category/{elementId}'      => 'deleteRelationProductMaterial2PriceCategory',
        ],
        'pluralize'     => false,
    ];
