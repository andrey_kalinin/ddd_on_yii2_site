<?php

return
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => [
            'catalog/furniture-type-template',
        ],
        'pluralize'  => false,
    ];
