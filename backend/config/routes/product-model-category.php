<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'catalog/product-model-category',
        ],
        'tokens'        => [
            '{id}'        => '<id:\\d+>',
        ],
        'extraPatterns' => [
        ],
        'pluralize'     => false,
    ];