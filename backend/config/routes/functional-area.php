<?php

return
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => [
            'catalog/functional-area',
        ],
        'pluralize'  => false,
    ];
