<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'crm/employee',
            ],
            'tokens'        => [
                '{id}'        => '<id:\\d+>',
                '{elementId}' => '<elementId:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/addresses'            => 'viewAddresses',
                'GET {id}/user-role'            => 'viewUserRole',
                'GET {id}/product-offers'       => 'viewProductOffers',
                'GET {id}/company'              => 'viewCompany',
                'GET {id}/access-specification' => 'viewAccessSpecification',

                'POST {id}/address/{elementId}'   => 'createRelationEmployee2Address',
                'POST {id}/user-role/{elementId}' => 'createRelationEmployee2UserRole',
                'POST {id}/company/{elementId}'   => 'createRelationEmployee2Company',

                'DELETE {id}/address/{elementId}'   => 'deleteRelationEmployee2Address',
                'DELETE {id}/user-role/{elementId}' => 'deleteRelationEmployee2UserRole',
                'DELETE {id}/company/{elementId}'   => 'deleteRelationEmployee2Company',
            ],
            'pluralize'     => false,
];
