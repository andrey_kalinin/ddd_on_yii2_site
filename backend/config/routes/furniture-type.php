<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'catalog/furniture-type',
        ],
        'tokens'        => [
            '{id}' => '<id:\\d+>',
        ],
        'extraPatterns' => [
            'GET {id}/template' => 'viewTemplate',
        ],
        'pluralize'     => false,
    ];
