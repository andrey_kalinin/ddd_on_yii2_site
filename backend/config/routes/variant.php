<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'catalog/variant',
        ],
        'tokens'        => [
            '{id}'        => '<id:\\d+>',
            '{elementId}' => '<elementId:\\d+>',
        ],
        'extraPatterns' => [
            'GET {id}/option'                => 'viewOption',
            'POST {id}/option/{elementId}'   => 'createRelationVariant2Option',
            'DELETE {id}/option/{elementId}' => 'deleteRelationVariant2Option',
        ],
        'pluralize'     => false,
    ];