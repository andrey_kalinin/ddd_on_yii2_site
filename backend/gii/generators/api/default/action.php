<?php
/* @var $generator api\gii\generators\api\Generator */

echo '<?php' . PHP_EOL;
$action = $generator->getTypeOfClassMethod($generator->action);
$element = false;
$plural = false;

if (preg_match('`^(bind|unbind)`',$action)) {
    $element = true;
}
// множественное число
if (substr($action,-1) == 's') {
    $plural = true;
}
if (substr($action,-17) == 'HierarchyChildren') {
    $plural = true;
}
if (substr($action,-15) == 'HierarchyParent') {
    $plural = true;
}

?>

namespace common\extendedStdComponents\<?= lcfirst($generator->entityContext) ?>\<?= lcfirst($generator->entityName) ?>;

use commonprj\components\<?= $generator->entityContext ?>\entities\<?= lcfirst($generator->entityName) ?>\<?= ucfirst($generator->entityName) ?>;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class <?= ucfirst($generator->entityName) ?>
 * @package api\controllers
 */
class <?= ucfirst($generator->action) ?>Action extends BaseAction
{

    /**
     * @param int $id
<?php if (!$plural){ ?>
     * @param int $elementId
     * @return bool
<?php } else { ?>
     * @return array
<?php } ?>
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id<?php if ($element){ ?>, int $elementId<?php } ?>)<?php if ($plural){ ?>: array<?php } ?> 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var <?= ucfirst($generator->entityName) ?> $entity
         */
        $entity = $this->findModel($id);

        return $entity-><?= $action ?>(<?php if ($element){ ?>$elementId<?php } ?>)<?php if ($plural){ ?> ?? []<?php } ?>;
    }

}