<?php

use commonprj\components\core\entities as CoreEntities;
use \commonprj\components\catalog\entities as CatalogEntities;
use \commonprj\components\crm\entities as CrmEntities;


return [
    'aliases'    => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache'                        => [
            'class' => 'yii\caching\FileCache',
        ],
        'request'                      => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'propertyRepository'           => [
            'class' => CoreEntities\property\PropertyServiceRepository::class,
        ],
        'propertyTreeItemRepository'           => [
            'class' => CoreEntities\PropertyTreeItem\PropertyTreeItemServiceRepository::class,
        ],
        'propertyListItemRepository'           => [
            'class' => CoreEntities\propertyListItem\PropertyListItemServiceRepository::class,
        ],
        'propertyVariantRepository'           => [
            'class' => CoreEntities\propertyVariant\PropertyVariantServiceRepository::class,
        ],
        'elementCategoryRepository'           => [
            'class' => CoreEntities\elementCategory\ElementCategoryServiceRepository::class,
        ],
        'templateRepository'           => [
            'class' => CoreEntities\template\TemplateServiceRepository::class,
        ],
        'productModelRepository'       => [
            'class' => CatalogEntities\productModel\ProductModelServiceRepository::class,
        ],
        'elementRepository'            => [
            'class' => CoreEntities\element\ElementServiceRepository::class,
        ],
        'propertyTypeRepository'       => [
            'class' => CoreEntities\propertyType\PropertyTypeServiceRepository::class,
        ],
        'elementClassRepository'       => [
            'class' => CoreEntities\elementClass\ElementClassServiceRepository::class,
        ],
        'propertyGroupRepository'      => [
            'class' => CoreEntities\propertyGroup\PropertyGroupServiceRepository::class,
        ],
        'propertyUnitRepository'       => [
            'class' => CoreEntities\propertyUnit\PropertyUnitServiceRepository::class,
        ],
        'materialGroupRepository'      => [
            'class' => CatalogEntities\materialGroup\MaterialGroupServiceRepository::class,
        ],
        'materialTemplateRepository'   => [
            'class' => CatalogEntities\materialTemplate\MaterialTemplateServiceRepository::class,
        ],
        'materialRepository'           => [
            'class' => CatalogEntities\material\MaterialServiceRepository::class,
        ],
        'priceCategoryRepository'      => [
            'class' => CatalogEntities\priceCategory\PriceCategoryServiceRepository::class,
        ],
        'materialCollectionRepository' => [
            'class' => CatalogEntities\materialCollection\MaterialCollectionServiceRepository::class,
        ],
        'productMaterialRepository'    => [
            'class' => CatalogEntities\productMaterial\ProductMaterialServiceRepository::class,
        ],

        // CRM
        'manufacturerRepository'       => [
            'class' => CrmEntities\manufacturer\ManufacturerServiceRepository::class,
        ],
        'sellerRepository'             => [
            'class' => CrmEntities\seller\SellerServiceRepository::class,
        ],
        'addressRepository'            => [
            'class' => CrmEntities\address\AddressServiceRepository::class,
        ],
        'employeeRepository'           => [
            'class' => CrmEntities\employee\EmployeeServiceRepository::class,
        ],
        'customerRepository'           => [
            'class' => CrmEntities\customer\CustomerServiceRepository::class,
        ],
        'companyRepository'            => [
            'class' => CrmEntities\company\CompanyServiceRepository::class,
        ],

        'optionRepository'         => [
            'class' => CatalogEntities\Option\OptionServiceRepository::class,
        ],
        'variantRepository'         => [
            'class' => CatalogEntities\Variant\VariantServiceRepository::class,
        ],
        // Services
        'propertyService'              => [
            'class' => \commonprj\services\PropertyService::class,
        ],
        'storeService'                 => [
            'class' => \commonprj\services\StoreService::class,
        ],
    ],
];


