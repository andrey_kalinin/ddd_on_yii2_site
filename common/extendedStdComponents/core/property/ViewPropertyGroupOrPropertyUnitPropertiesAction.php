<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 11:24
 */

namespace common\extendedStdComponents\core\property;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewPropertyGroupOrPropertyUnitPropertiesAction
 * @package common\extendedStdComponents
 */
class ViewPropertyGroupOrPropertyUnitPropertiesAction extends BaseAction
{
    /**
     * @param $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        $entity = $this->findModel($id);

        return $entity->getProperties($entity);
    }
}