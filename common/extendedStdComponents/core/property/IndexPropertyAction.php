<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06.03.2018
 * Time: 17:13
 */

namespace common\extendedStdComponents\core\property;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\propertyType\PropertyType;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * Class IndexPropertyAction
 * @package common\extendedStdComponents\property
 */
class IndexPropertyAction extends BaseAction
{
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $queryParams = Yii::$app->getRequest()->getQueryParams();

        /** @var BaseCrudModel $modelClass */
        $modelClass = new $this->modelClass();

        $condition = $queryParams;

        //   Массив
        $propertyTypeCondition = ["parentId__eq" => Property::PROPERTY_TYPE_PROPERTY_ID];
        $propertyTypeResponse = PropertyType::findAll($propertyTypeCondition);
        $propertyTypeIds = [];

        foreach ((array)$propertyTypeResponse['items'] as $propertyType) {
            $propertyTypeIds[] = $propertyType['id'];
        }

        if (isset($queryParams['propertyTypeId__eq'])) {
            $propertyTypeIds = array_intersect($propertyTypeIds, [$queryParams['propertyTypeId__eq']]);
        }

        unset($condition['propertyTypeId__eq']);
        $condition['propertyTypeId__in'] = implode(',', $propertyTypeIds);

        return $modelClass->findAll($condition);
    }
}