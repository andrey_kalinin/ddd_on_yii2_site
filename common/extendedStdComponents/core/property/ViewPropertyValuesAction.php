<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 18:26
 */

namespace common\extendedStdComponents\core\property;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewPropertyValuesAction
 * @package common\extendedStdComponents\property
 */
class ViewPropertyValuesAction extends BaseAction
{
    /**
     * @param $id
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        /**
         * @var Property $entity
         */
        $entity = $this->findModel($id);

        return $entity->getValues($entity);
    }
}