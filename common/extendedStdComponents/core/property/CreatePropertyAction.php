<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.02.2018
 * Time: 19:16
 */

namespace common\extendedStdComponents\core\property;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * Class CreatePropertyAction
 * @package common\extendedStdComponents\property
 */
class CreatePropertyAction extends BaseAction
{
    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var Property $model */
        $attributes = Yii::$app->getRequest()->getBodyParams();
        $model = (isset($attributes['id'])) ? $this->findModel($attributes['id']) : new $this->modelClass();


        $modelAttributes = $attributes;
        if (isset($modelAttributes['items'])) {
            unset($modelAttributes['items']);
        }
        $model->setAttributes($modelAttributes, false);

        if (!empty($attributes['items']) && $model->propertyTypeId == Property::PROPERTY_TYPE_LIST) {

            $listItems = [];

            foreach ((array)$attributes['items'] as $listItem) {
                $listItems[$listItem['item']] = $listItem['label'];
            }

            $result = $model->saveWithListItems($listItems);
        } else {
            $result = $model->save();
        }

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $model->id);

        return $result;
    }
}