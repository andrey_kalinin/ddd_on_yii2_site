<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 19:26
 */

namespace common\extendedStdComponents\core\elementClass;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewElementClassPropertiesAction
 * @package common\extendedStdComponents\elementClass
 */
class ViewElementClassPropertiesAction extends BaseAction
{
    /**
     * @param $id
     * @return array
     * @throws \yii\web\HttpException
     */
    public function run($id)
    {
        /**
         * @var ElementClass $entity
         */
        $entity = $this->findModel($id);

        return $entity->getProperties();
    }
}