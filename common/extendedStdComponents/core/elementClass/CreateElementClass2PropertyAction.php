<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 19:27
 */

namespace common\extendedStdComponents\core\elementClass;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

class CreateElementClass2PropertyAction extends BaseAction
{
    /**
     * @param $id
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        /**
         * @var ElementClass $entity
         */
        $entity = $this->findModel($id);

        $propertyId = Yii::$app->request->getQueryParam('propertyId');

        return $entity->bindProperty($propertyId);
    }
}