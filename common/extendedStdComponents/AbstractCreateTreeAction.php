<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.04.2018
 * Time: 15:35
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * Class AbstractCreateTreeAction
 * @package common\extendedStdComponents
 */
abstract class AbstractCreateTreeAction extends AbstractBackendAction
{
    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $params = Yii::$app->getRequest()->getBodyParams();
        $params['propertyId'] = $this->getPropertyId();

        /** @var BaseCrudModel $model */
        $model = new $this->modelClass();
        $model->load($params, '');
        $result = $model->save();

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $model->id);

        return $result;
    }
}