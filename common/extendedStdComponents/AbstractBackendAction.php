<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11.04.2018
 * Time: 13:46
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;

abstract class AbstractBackendAction extends BaseAction
{
    const PROPERTY_SYSNAME = '';

    /**
     * @return mixed
     */
    protected function getPropertyId()
    {
        $properties = Property::findAll(['sysname__eq' => static::PROPERTY_SYSNAME]);
        $property = reset($properties['items']);
        return $property->id;
    }
}