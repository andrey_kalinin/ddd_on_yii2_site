<?php

namespace common\extendedStdComponents\crm\employee;

use commonprj\components\crm\entities\employee\Employee;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Employee * @package api\controllers
 */
class DeleteRelationEmployee2CompanyAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Employee $entity
         */
        $entity = $this->findModel($id);

        return $entity->unbindCompany($elementId);
    }

}