<?php

namespace common\extendedStdComponents\crm\manufacturer;

use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Manufacturer * @package api\controllers
 */
class ViewProductModelsAction extends BaseAction
{

    /**
     * @param int $id
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id): array 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Manufacturer $entity
         */
        $entity = $this->findModel($id);

        return $entity->getProductModels() ?? [];
    }

}