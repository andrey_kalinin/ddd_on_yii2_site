<?php

namespace common\extendedStdComponents\catalog\productMaterial;

use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ProductMaterial * @package api\controllers
 */
class DeleteRelationProductMaterial2PriceCategoryAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ProductMaterial $entity
         */
        $entity = $this->findModel($id);

        return $entity->unbindPriceCategory($elementId);
    }

}