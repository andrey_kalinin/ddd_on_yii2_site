<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.03.2018
 * Time: 19:02
 */

namespace common\extendedStdComponents\catalog\productMaterial;

use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\web\HttpException;

class CreateProductMaterialAction extends BaseAction
{
    private const REQUIRED_PARAMS = [
        'priceCategoryId',
        'materialCollectionId',
        'materialId',
        'materialGroupId',
    ];

    /**
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $storeService = Yii::$app->storeService;
        $manufacturerId = $storeService->getCurrentCompanyId();

        if (is_null($manufacturerId)) {
            throw new HttpException(400, "Request has no `manufacturerId`");
        }

        $requiredParams = $this->getRequiredParams();


        /**
         * @var ProductMaterial $productMaterial
         */
        $productMaterial = new ProductMaterial();
        $productMaterial->load(Yii::$app->getRequest()->getBodyParams(), '');
        $productMaterial->save();

        try {
            $productMaterial->bindPriceCategory($requiredParams['priceCategoryId']);
            $productMaterial->bindMaterialCollection($requiredParams['materialCollectionId']);
            $productMaterial->bindMaterial($requiredParams['materialId']);
            $productMaterial->bindMaterialGroup($requiredParams['materialGroupId']);
            $productMaterial->bindManufacturer($manufacturerId);
        } catch (\Exception $e) {
            $productMaterial->delete();
            throw $e;
        }

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $productMaterial->id);

        return true;
    }

    /**
     * @return array
     * @throws HttpException
     */
    private function getRequiredParams(): array
    {
        $result = [];

        foreach (self::REQUIRED_PARAMS as $name){
            $value = Yii::$app->getRequest()->getBodyParam($name);

            if (is_null($value)) {
                throw new HttpException(400, "Request has no `{$name}` params");
            }

            $result[$name] = $value;
        }

        return $result;
    }
}