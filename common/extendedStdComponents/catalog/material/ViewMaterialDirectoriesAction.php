<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.03.2018
 * Time: 13:57
 */

namespace common\extendedStdComponents\catalog\material;

use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewMaterialDirectoriesAction
 * @package common\extendedStdComponents\material
 */
class ViewMaterialDirectoriesAction extends BaseAction
{
    /**
     * @return array
     */
    public function run()
    {
        $models = [
            'materialGroups' => (new MaterialGroup()),
        ];

        $result = [];

        foreach ($models as $key =>$model) {
            $response = $model->repository->findAll($model, ['get-all' => 1]);
            $result[$key] = $response;
        }

        return $result;
    }
}