<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.03.2018
 * Time: 17:10
 */

namespace common\extendedStdComponents\catalog\material;

use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * Class CreateAndBindMaterialTemplate
 * @package common\extendedStdComponents\material
 */
class CreateAndBindMaterialTemplateAction extends BaseAction
{

    /**
     * @param int $id
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Material $entity
         */
        $entity = $this->findModel($id);
        $materialTemplateId = Yii::$app->getRequest()->getBodyParam('id') ?? null;
        $properties = Yii::$app->getRequest()->getBodyParam('relatedProperties');

        $params = [];

        foreach ($properties as $property) {
            $params['properties']['properties'][] = $property['id'];
        }

        if ($materialTemplateId) {
            $materialTemplate = MaterialTemplate::findOne($materialTemplateId);
            $materialTemplate->load($params, '');
            $result = $materialTemplate->save();
        } else {
            $materialTemplate = new MaterialTemplate();
            $materialTemplate->load($params, '');
            $result = $materialTemplate->save();

            $response = Yii::$app->response;
            $response->setStatusCode(201);
            $response->headers->add('id', $materialTemplate->id);

            return $entity->bindMaterialTemplate($materialTemplate->id);
        }

        return $result;
    }
}
