<?php

namespace common\extendedStdComponents\catalog\variant;

use commonprj\components\catalog\entities\variant\Variant;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Variant * @package api\controllers
 */
class DeleteRelationVariant2OptionAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Variant $entity
         */
        $entity = $this->findModel($id);

        return $entity->unbindOption($elementId);
    }

}