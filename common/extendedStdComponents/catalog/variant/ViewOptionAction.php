<?php

namespace common\extendedStdComponents\catalog\variant;

use commonprj\components\catalog\entities\variant\Variant;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Variant * @package api\controllers
 */
class ViewOptionAction extends BaseAction
{
    /**
     * @param int $id
     * @return \commonprj\components\catalog\entities\option\Option|null
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Variant $entity
         */
        $entity = $this->findModel($id);

        return $entity->getOption();
    }

}