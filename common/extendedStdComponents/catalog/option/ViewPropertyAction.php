<?php

namespace common\extendedStdComponents\catalog\option;

use commonprj\components\catalog\entities\option\Option;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Option * @package api\controllers
 */
class ViewPropertyAction extends BaseAction
{

    /**
     * @param int $id
     * @return \commonprj\components\core\entities\property\Property
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Option $entity
         */
        $entity = $this->findModel($id);

        return $entity->getProperty();
    }

}