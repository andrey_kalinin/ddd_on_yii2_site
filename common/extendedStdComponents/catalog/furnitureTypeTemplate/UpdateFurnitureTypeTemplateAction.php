<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05.04.2018
 * Time: 14:20
 */

namespace common\extendedStdComponents\catalog\furnitureTypeTemplate;

use commonprj\components\core\entities\propertyVariant\PropertyVariantServiceRepository;
use commonprj\components\core\entities\template\Template;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

class UpdateFurnitureTypeTemplateAction extends AbstractFurnitureTypeTemplateAction
{
    /**
     * @param int $id
     * @return null
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Template $template
         */
        $template = $this->findModel($id);

        $template->load([
            'propertyIds'    => Yii::$app->getRequest()->getBodyParam('propertyIds') ?? [],
            'elementClassId' => $this->getElementClassId(),
        ], '');
        $template->relatedProperties = null;
        $template->update();

        return null;
    }
}