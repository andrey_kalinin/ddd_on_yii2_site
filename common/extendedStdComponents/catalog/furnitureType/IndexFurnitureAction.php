<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.04.2018
 * Time: 16:34
 */

namespace common\extendedStdComponents\catalog\furnitureType;

use common\extendedStdComponents\AbstractIndexTreeAction;

/**
 * Class IndexFurnitureAction
 * @package common\extendedStdComponents\furnitureType
 */
class IndexFurnitureAction extends AbstractIndexTreeAction
{
    const PROPERTY_SYSNAME = 'furnitureType';
}