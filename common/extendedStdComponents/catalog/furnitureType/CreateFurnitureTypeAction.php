<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.04.2018
 * Time: 16:33
 */

namespace common\extendedStdComponents\catalog\furnitureType;

use common\extendedStdComponents\AbstractCreateTreeAction;

/**
 * Class CreateFurnitureTypeAction
 * @package common\extendedStdComponents\furnitureType
 */
class CreateFurnitureTypeAction extends AbstractCreateTreeAction
{
    const PROPERTY_SYSNAME = 'furnitureType';
}