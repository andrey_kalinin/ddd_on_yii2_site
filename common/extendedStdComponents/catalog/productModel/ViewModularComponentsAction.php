<?php

namespace common\extendedStdComponents\catalog\productModel;

use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ProductModel * @package api\controllers
 */
class ViewModularComponentsAction extends BaseAction
{

    /**
     * @param int $id
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id): array 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ProductModel $entity
         */
        $entity = $this->findModel($id);

        return $entity->getModularComponents() ?? [];
    }

}