<?php

namespace common\extendedStdComponents\catalog\productModel;

use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ProductModel * @package api\controllers
 */
class DeleteRelationProductModel2ConstructionElementAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ProductModel $entity
         */
        $entity = $this->findModel($id);

        return $entity->unbindConstructionElement($elementId);
    }

}