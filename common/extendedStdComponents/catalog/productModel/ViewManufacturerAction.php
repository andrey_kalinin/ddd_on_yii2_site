<?php

namespace common\extendedStdComponents\catalog\productModel;

use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ProductModel * @package api\controllers
 */
class ViewManufacturerAction extends BaseAction
{

    /**
     * @param int $id
     * @return \commonprj\components\crm\entities\manufacturer\Manufacturer|null
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ProductModel $entity
         */
        $entity = $this->findModel($id);

        return $entity->getManufacturer();
    }

}