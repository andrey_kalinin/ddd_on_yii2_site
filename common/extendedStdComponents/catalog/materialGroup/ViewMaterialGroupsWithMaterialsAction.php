<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.04.2018
 * Time: 13:27
 */

namespace common\extendedStdComponents\catalog\materialGroup;

use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewMaterialGroupsWithMaterialsAction
 * @package common\extendedStdComponents\catalog\materialGroup
 */
class ViewMaterialGroupsWithMaterialsAction extends BaseAction
{
    /**
     * @return MaterialGroup[]|static[]
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var MaterialGroup[] $materialGroups */
        $materialGroups = MaterialGroup::findAll(['get-all' => 1]);

        foreach ($materialGroups as &$materialGroup) {
            /** @var Material[] $materials */
            $materials = $materialGroup->getMaterials();
            $materialGroup->materials = $materials;
        }

        return $materialGroups;
    }
}