<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.04.2018
 * Time: 16:13
 */

namespace common\extendedStdComponents\catalog\functionalArea;

use common\extendedStdComponents\AbstractIndexTreeAction;

/**
 * Class ViewFunctionalAction
 * @package common\extendedStdComponents\catalog\functionalArea
 */
class IndexFunctionalAction extends AbstractIndexTreeAction
{
    const PROPERTY_SYSNAME = 'functionalArea';
}