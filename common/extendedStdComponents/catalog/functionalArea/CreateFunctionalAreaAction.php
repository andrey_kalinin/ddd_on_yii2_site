<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.04.2018
 * Time: 15:37
 */

namespace common\extendedStdComponents\catalog\functionalArea;

use common\extendedStdComponents\AbstractCreateTreeAction;

/**
 * Class CreateFunctionalAreaAction
 * @package common\extendedStdComponents\catalog\functionalArea
 */
class CreateFunctionalAreaAction extends AbstractCreateTreeAction
{
    const PROPERTY_SYSNAME = 'functionalArea';
}