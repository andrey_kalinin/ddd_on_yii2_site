<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.04.2018
 * Time: 12:21
 */

namespace common\extendedStdComponents\catalog\materialCollection;


use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\web\HttpException;

/**
 * Class CreateMaterialCollectionAction
 * @package common\extendedStdComponents\materialCollection
 */
class CreateMaterialCollectionAction extends BaseAction
{
    /**
     * @return bool
     * @throws HttpException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $storeService = Yii::$app->storeService;
        $manufacturerId = $storeService->getCurrentCompanyId();

        if (is_null($manufacturerId)) {
            throw new HttpException(400, "Request has no `manufacturerId`");
        }

        /**
         * @var MaterialCollection $materialCollection
         */
        $materialCollection = new MaterialCollection();
        $materialCollection->load(Yii::$app->getRequest()->getBodyParams(), '');
        $materialCollection->save();

        try {
            $materialCollection->bindManufacturer($manufacturerId);
        } catch (\Exception $e) {
            $materialCollection->delete();
            throw $e;
        }

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $materialCollection->id);

        return true;
    }
}