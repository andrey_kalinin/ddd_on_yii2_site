<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.04.2018
 * Time: 11:59
 */

namespace common\extendedStdComponents\catalog\priceCategory;

use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\web\HttpException;

/**
 * Class CreatePriceCategoryAction
 * @package common\extendedStdComponents\priceCategory
 */
class CreatePriceCategoryAction extends BaseAction
{
    /**
     * @return bool
     * @throws HttpException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $storeService = Yii::$app->storeService;
        $manufacturerId = $storeService->getCurrentCompanyId();

        if (is_null($manufacturerId)) {
            throw new HttpException(400, "Request has no `manufacturerId`");
        }

        /**
         * @var PriceCategory $priceCategory
         */
        $priceCategory = new PriceCategory();
        $priceCategory->load(Yii::$app->getRequest()->getBodyParams(), '');
        $priceCategory->save();

        try {
            $priceCategory->bindManufacturer($manufacturerId);
        } catch (\Exception $e) {
            $priceCategory->delete();
            throw $e;
        }

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $priceCategory->id);

        return true;
    }
}