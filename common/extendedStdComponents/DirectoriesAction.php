<?php

/**
 * Created by daSilva.Rodrigues
 * Date: 05.07.2016
 */

namespace common\extendedStdComponents;

use commonprj\extendedStdComponents\BaseAction;


/**
 * DeleteAction implements the API endpoint for deleting a model.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DirectoriesAction extends BaseAction
{
    public $directories = [];

    /**
     * @return array
     */
    public function run()
    {
        return $this->buildDirectories();
    }

    protected function buildDirectories()
    {
        $result = [];

        foreach ((array)$this->directories as $key => $directory) {
            if (is_callable($directory)) {
                $values = call_user_func($directory);
            } else {
                $response = $directory::findAll(['get-all' => 1]);
                $values = $response['items'] ?? $response;
            }
            $result[$key] = $values;
        }

        return $result;
    }
}